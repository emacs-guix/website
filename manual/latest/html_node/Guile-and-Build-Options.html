<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This document describes Emacs-Guix, the Emacs interface for the
https://www.gnu.org/software/guix/ (GNU Guix) package manager.

Copyright (C) 2014-2019, 2021 Alex Kost

Copyright (C) 2018 Oleg Pykhalov

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is available at
http://www.gnu.org/licenses/fdl.html.
 -->
<!-- Created by GNU Texinfo 6.7, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Guile and Build Options (Emacs-Guix Reference Manual)</title>

<meta name="description" content="Guile and Build Options (Emacs-Guix Reference Manual)">
<meta name="keywords" content="Guile and Build Options (Emacs-Guix Reference Manual)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html" rel="start" title="Top">
<link href="Concept-Index.html" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Configuration.html" rel="up" title="Configuration">
<link href="List_002fInfo-Configuration.html" rel="next" title="List/Info Configuration">
<link href="Graph-Configuration.html" rel="prev" title="Graph Configuration">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="../../../css/manual.css">


</head>

<body lang="en">
<span id="Guile-and-Build-Options"></span><div class="header">
<p>
Next: <a href="List_002fInfo-Configuration.html" accesskey="n" rel="next">List/Info Configuration</a>, Previous: <a href="Graph-Configuration.html" accesskey="p" rel="prev">Graph Configuration</a>, Up: <a href="Configuration.html" accesskey="u" rel="up">Configuration</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<span id="Guile-and-Build-Options-1"></span><h3 class="section">15.2 Guile and Build Options</h3>

<dl compact="compact">
<dd>
<span id="index-guix_002dguile_002dprogram"></span>
</dd>
<dt><code>guix-guile-program</code></dt>
<dd><p>If you have some special needs for starting a Guile process, you may
set this variable.  For example, by default, Guile is started with
<code>--no-auto-compile</code> flag (because auto-compilation may take a
very long time), so if you just want <code>guile</code> without any flags,
you may just use:
</p>
<div class="example">
<pre class="example">(setq guix-guile-program &quot;guile&quot;)
</pre></div>

<span id="index-guix_002dload_002dpath"></span>
<span id="index-guix_002dload_002dcompiled_002dpath"></span>
</dd>
<dt><code>guix-load-path</code></dt>
<dt><code>guix-load-compiled-path</code></dt>
<dd><p>Directory or a list of directories prepended to Guile&rsquo;s
<code>%load-path</code> and <code>%load-compiled-path</code> (see <a href="https://www.gnu.org/software/guile/manual/html_node/Load-Paths.html#Load-Paths">Load Paths</a> in <cite>The GNU Guile Reference Manual</cite>).
</p>
<p>If you use Guix from a git checkout (see <a href="https://www.gnu.org/software/guix/manual/html_node/Running-Guix-Before-It-Is-Installed.html#Running-Guix-Before-It-Is-Installed">Running Guix Before It Is
Installed</a> in <cite>The GNU Guix Reference Manual</cite>), you may want
Emacs-Guix to use the same guix from git.  All you need is to point
Emacs-Guix to this checkout:
</p>
<div class="example">
<pre class="example">(setq guix-load-path &quot;/path/to/guix-git-dir&quot;)
</pre></div>

<p>Note that setting <code>guix-load-compiled-path</code> is not needed when
Scheme (<code>.scm</code>) and compiled (<code>.go</code>) files are placed in the
same directories.
</p>
<span id="index-guix_002duse_002dsubstitutes"></span>
</dd>
<dt><code>guix-use-substitutes</code></dt>
<dd><p>Has the same meaning as <code>--no-substitutes</code> option
(see <a href="https://www.gnu.org/software/guix/manual/html_node/Invoking-guix-build.html#Invoking-guix-build">Invoking guix build</a> in <cite>The GNU Guix Reference
Manual</cite>)&mdash;i.e., when non-nil, substitutes are enabled.
</p>
<span id="index-guix_002ddry_002drun"></span>
</dd>
<dt><code>guix-dry-run</code></dt>
<dd><p>Has the same meaning as <code>--dry-run</code> option (see <a href="https://www.gnu.org/software/guix/manual/html_node/Invoking-guix-build.html#Invoking-guix-build">Invoking guix
build</a> in <cite>The GNU Guix Reference Manual</cite>)&mdash;i.e., when non-nil,
do not build the derivations.
</p>
<span id="index-Guix-REPL-1"></span>
<span id="index-guix_002drepl_002duse_002dserver"></span>
</dd>
<dt><code>guix-repl-use-server</code></dt>
<dd><p>By default, along with the main Guix REPL, an additional (internal)
REPL is started.  This allows you to display packages, generations and
to receive other info from the Scheme side, while there is some active
process in the main Guix REPL (e.g., while downloading or building
packages).  If you don&rsquo;t want to have the second REPL, set this
variable to nil.
</p>
<span id="index-guix_002drepl_002duse_002dlatest"></span>
</dd>
<dt><code>guix-repl-use-latest</code></dt>
<dd><p>Set this variable to nil, if you don&rsquo;t want to use the latest Guix
code received with <code>guix pull</code> command (see <a href="https://www.gnu.org/software/guix/manual/html_node/Invoking-guix-pull.html#Invoking-guix-pull">Invoking guix
pull</a> in <cite>The GNU Guix Reference Manual</cite>).
</p>
</dd>
</dl>

<hr>
<div class="header">
<p>
Next: <a href="List_002fInfo-Configuration.html" accesskey="n" rel="next">List/Info Configuration</a>, Previous: <a href="Graph-Configuration.html" accesskey="p" rel="prev">Graph Configuration</a>, Up: <a href="Configuration.html" accesskey="u" rel="up">Configuration</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html" title="Index" rel="index">Index</a>]</p>
</div>



</body>
</html>
