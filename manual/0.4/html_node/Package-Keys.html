<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<!-- This document describes Emacs-Guix, the Emacs interface for the
https://www.gnu.org/software/guix/ (GNU Guix) package manager.

Copyright (C) 2014-2018 Alex Kost

Copyright (C) 2018 Oleg Pykhalov

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is available at
http://www.gnu.org/licenses/fdl.html.
 -->
<!-- Created by GNU Texinfo 6.5, http://www.gnu.org/software/texinfo/ -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Package Keys (Emacs-Guix Reference Manual)</title>

<meta name="description" content="Package Keys (Emacs-Guix Reference Manual)">
<meta name="keywords" content="Package Keys (Emacs-Guix Reference Manual)">
<meta name="resource-type" content="document">
<meta name="distribution" content="global">
<meta name="Generator" content="texi2any">
<link href="index.html#Top" rel="start" title="Top">
<link href="Concept-Index.html#Concept-Index" rel="index" title="Concept Index">
<link href="index.html#SEC_Contents" rel="contents" title="Table of Contents">
<link href="Packages.html#Packages" rel="up" title="Packages">
<link href="Package-Locations.html#Package-Locations" rel="next" title="Package Locations">
<link href="Package-Commands.html#Package-Commands" rel="prev" title="Package Commands">
<style type="text/css">
<!--
a.summary-letter {text-decoration: none}
blockquote.indentedblock {margin-right: 0em}
blockquote.smallindentedblock {margin-right: 0em; font-size: smaller}
blockquote.smallquotation {font-size: smaller}
div.display {margin-left: 3.2em}
div.example {margin-left: 3.2em}
div.lisp {margin-left: 3.2em}
div.smalldisplay {margin-left: 3.2em}
div.smallexample {margin-left: 3.2em}
div.smalllisp {margin-left: 3.2em}
kbd {font-style: oblique}
pre.display {font-family: inherit}
pre.format {font-family: inherit}
pre.menu-comment {font-family: serif}
pre.menu-preformatted {font-family: serif}
pre.smalldisplay {font-family: inherit; font-size: smaller}
pre.smallexample {font-size: smaller}
pre.smallformat {font-family: inherit; font-size: smaller}
pre.smalllisp {font-size: smaller}
span.nolinebreak {white-space: nowrap}
span.roman {font-family: initial; font-weight: normal}
span.sansserif {font-family: sans-serif; font-weight: normal}
ul.no-bullet {list-style: none}
-->
</style>
<link rel="stylesheet" type="text/css" href="../../../css/manual.css">


</head>

<body lang="en">
<a name="Package-Keys"></a>
<div class="header">
<p>
Next: <a href="Package-Locations.html#Package-Locations" accesskey="n" rel="next">Package Locations</a>, Previous: <a href="Package-Commands.html#Package-Commands" accesskey="p" rel="prev">Package Commands</a>, Up: <a href="Packages.html#Packages" accesskey="u" rel="up">Packages</a> &nbsp; [<a href="index.html#SEC_Contents" title="Table of contents" rel="contents">Contents</a>][<a href="Concept-Index.html#Concept-Index" title="Index" rel="index">Index</a>]</p>
</div>
<hr>
<a name="Package-Keys-1"></a>
<h4 class="subsection">3.1.2 Package Keys</h4>

<a name="List-Buffer"></a>
<h4 class="subsubsection">3.1.2.1 List Buffer</h4>

<p>Along with the general &ldquo;list&rdquo; keys (see <a href="List-buffer.html#List-buffer">List buffer</a>), a
&ldquo;package-list&rdquo; buffer additionally provides the following key
bindings:
</p>
<dl compact="compact">
<dt><kbd>i</kbd></dt>
<dd><p>Mark the current package for installation.
</p></dd>
<dt><kbd>d</kbd></dt>
<dd><p>Mark the current package for deletion.
</p></dd>
<dt><kbd>U</kbd></dt>
<dd><p>Mark the current package for upgrading.
</p></dd>
<dt><kbd>^</kbd></dt>
<dd><p>Mark all obsolete packages for upgrading (with prefix, mark all
installed packages for upgrading).
</p></dd>
<dt><kbd>x</kbd></dt>
<dd><p>Execute actions on the marked packages.
</p></dd>
<dt><kbd>e</kbd></dt>
<dd><p>Edit the definition of the current package (go to its location).  This
is similar to <code>guix edit</code> command (see <a href="http://www.gnu.org/software/guix/manual/html_node/Invoking-guix-edit.html#Invoking-guix-edit">Invoking guix
edit</a> in <cite>The GNU Guix Reference Manual</cite>), but for opening a
package recipe in the current Emacs instance.
</p></dd>
<dt><kbd>G</kbd></dt>
<dd><a name="index-graph"></a>
<p>Show graph for the current package (see <a href="Graph-Configuration.html#Graph-Configuration">Graph Configuration</a>).
</p></dd>
<dt><kbd>z</kbd></dt>
<dd><p>Show package size (see <a href="http://www.gnu.org/software/guix/manual/html_node/Invoking-guix-size.html#Invoking-guix-size">Invoking guix size</a> in <cite>The GNU Guix
Reference Manual</cite>).
</p></dd>
<dt><kbd>L</kbd></dt>
<dd><p>Lint the current package (see <a href="http://www.gnu.org/software/guix/manual/html_node/Invoking-guix-lint.html#Invoking-guix-lint">Invoking guix lint</a> in <cite>The GNU
Guix Reference Manual</cite>).  With prefix argument, you&rsquo;ll be prompted for
checker names.
</p></dd>
<dt><kbd>B</kbd></dt>
<dd><p>Display latest builds of the current package (see <a href="Hydra.html#Hydra">Hydra</a>).
</p></dd>
</dl>

<a name="Info-Buffer"></a>
<h4 class="subsubsection">3.1.2.2 Info Buffer</h4>

<p>Similarly, &ldquo;package-info&rdquo; buffer provides the following key bindings:
</p>
<dl compact="compact">
<dt><kbd>i</kbd></dt>
<dd><p>Install the current package.
</p></dd>
<dt><kbd>d</kbd></dt>
<dd><p>Delete the current package.
</p></dd>
<dt><kbd>U</kbd></dt>
<dd><p>Upgrade the current package.
</p></dd>
<dt><kbd>e</kbd></dt>
<dd><p>Go to the package definition.
</p></dd>
<dt><kbd>G</kbd></dt>
<dd><p>Show package graph.
</p></dd>
<dt><kbd>z</kbd></dt>
<dd><p>Show package size.
</p></dd>
<dt><kbd>L</kbd></dt>
<dd><p>Lint the package.
</p></dd>
</dl>




</body>
</html>
