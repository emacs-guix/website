;;; releases.scm --- Releases page

;; Copyright © 2018–2019 Alex Kost <alezost@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (site releases)
  #:use-module (ice-9 match)
  #:use-module (site shared)
  #:use-module (site utils)
  #:export (latest-tarball
            releases-page))

(define %tarball-sums
  '(
    ("0.5.2"   "fa1e5aecadef9627eee49e45b4240ada3f6643261305a7c814f179f20123e67b")
    ("0.5.1.1" "0354b3c6940569833eb60860b1afa84c3420e3174b80269e174cda61153dafbf")
    ("0.5.1"   "b04fee2bb5670f524c611148e4e4c68bd5a57f631cd210f6faa2fc8b408491bf")
    ("0.5"     "217a0fd386b6f99978434bf2c97b19b02be822001475565fbc7993633a6afd27")
    ("0.4.1.1" "a5a4c07295edfba1f665a3ac49d211761b05082dfc3e9b418147cff515cb7649")
    ("0.4.1"   "44a92eec6c8515e9158a669fb62563d68480ba4f3ce16c57039162eeb6fd7051")
    ("0.4"     "18d71c09aafa60a3d04ed8fe5df1c86179f4cf8df95befc804af24d41e58c4da")
    ("0.3.4"   "4eeeb8ab1eb2f188f2b64fb360d2b2ffda62c2ab5915bf8eb3b63a3577725f94")
    ("0.3.3"   "aca623def7e19047dc8e0839c0c847270741a79cb9e429744b68904296174b56")
    ("0.3.2"   "93029de037f48798f7f7e149c843cb22ebe88ca8cee69ba88c3e718215edce2d")
    ("0.3.1"   "bffb27ebf32c8dba15c21d159fb604955c4c2c67abee45e3620ca9e53248fa68")
    ("0.3"     "791959f9ccdd2a29b0b6c7ac3a649a1464fe1842542a302763ea3040c2fd478c")
    ("0.2.2"   "93099ad37c71564cf240dcec82bc581ddc98e478c51c740eb3f5ed4e04f487c4")
    ("0.2.1"   "9e777c038f327fa2105eb160192550c87a09c7a155cd88fdcdce04d607031d5a")
    ("0.2"     "4517cc300e6a375d6bd260a3ceb12f8e6c73d03b8226127a64bb547552412640")))

(define (tarball-name version)
  "Return name of the release tarball for VERSION."
  (string-append "emacs-guix-" version ".tar.gz"))

(define (latest-tarball)
  "Return name of the latest release tarball."
  (tarball-name (caar %tarball-sums)))

(define (release-table)
  "Return table for the release tarballs."
  (define (table-row version sum)
    (let ((name (tarball-name version)))
      `(tr (td (a (@ (href ,(release-url name)))
                  ,name))
           (td ,sum)
           (td ,(emacs-guix-source-href
                 #:url-end (string-append "emacs-guix/tags/v" version)
                 #:title "news")))))

  `(table
    (thead (tr (th "tarball")
               (th "sha256sum")
               (th "release notes")))
    (tbody ,@(map (match-lambda
                    ((version sum)
                     (table-row version sum)))
                  %tarball-sums))))

(define (releases-page)
  "Return the releases page."
  `(html
    (@ (lang "en"))
    ,(head-tag "Releases")
    (body
     ,(page-header)
     (h2 "Releases")
     ,(release-table))))

;;; releases.scm ends here
